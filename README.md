## Synopsis

At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)

## Code Example

Show what the library does as concisely as possible, developers should be able to figure out **how** your project solves their problem by looking at the code example. Make sure the API you are showing off is obvious, and that your code is short and concise.

Within Intellij or Eclipse, goto src/java/com.example.tdd.berlinclock
and right click app.java to launch app, it should return the local time:
```
Current Local Time is : 18:10:09
0
R,R,R,0
R,R,R,0
Y,Y,0,0,0,0,0,0,0,0,0
0,0,0,0

Current Local Time is : 18:39:00
Y
R,R,R,0
R,R,R,0
Y,Y,R,Y,Y,R,Y,0,0,0,0
Y,Y,Y,Y
```
Tests are in test/java/com.example.tdd.berlinclock
BerlinClocktest - 100% methods, 100% lines covered
ColorTest - 100% methods, 100% lines covered
RowTest - 80% methods, 95% lines covered

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Installation
at root of project:
$ mvn clean install

Provide code examples and explanations of how to get the project.

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.

## License

A short snippet describing the license (MIT, Apache, etc.)