package com.example.tdd.berlinclock;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * Created by minminsanjose on 14/01/2016.
 */
public class app {
    private static BerlinClock berlinClock;
    public static void main(String[] args) {

        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("HH:mm:ss", Locale.US);
        LocalTime time = LocalTime.now();
        String f = formatter.format(time);
        System.out.println("Current Local Time is : " + f);

        //Set hrs:min:sec

        berlinClock = new BerlinClock();
        berlinClock.setHours(time.getHour());
        berlinClock.setMinutes(time.getMinute());
        berlinClock.setSeconds(time.getSecond());

        //Read SetTheoryClock
        System.out.println(berlinClock.getSecondsLamp().toString());
        System.out.println(berlinClock.getFiveHoursRow().toString());
        System.out.println(berlinClock.getOneHourRow().toString());
        System.out.println(berlinClock.getFiveMinutesRow().toString());
        System.out.println(berlinClock.getOneMinuteRow().toString());


    }
}
